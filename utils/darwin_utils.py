import pytest
import os
import csv
from constants import DATASET_FOLDER


class DarwinUtils:
	
	@staticmethod
	def load_data_set(file_name: str) -> csv:
		"""File Operation to load the Data set for the testing. """
		
		with open(os.path.join(DATASET_FOLDER, file_name)) as dataset_file:
			dataset = csv.reader(dataset_file, ',')
			return dataset
			
	def