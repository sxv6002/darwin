import pytest

from http import HTTPStatus
from client.darwin_client import DarwinClient
from constants import AUTH_LOGIN, AUTH_LOGIN_USER, USERNAME, PASSWORD


@pytest.mark.tags('smoke', 'darwin')
class TestDarwin:
	"""
	Testing Darwin Api urls with proper log in information.
	"""
	
	@pytest.fixture(scope="function", autouse=True)
	def setup(self, env: str):
		self.darwin_client = DarwinClient(env)

	@pytest.usefixture('load_dataset')
	def test_login_user(self, load_dataset):
		"""
		Testing Darwin /auth/login/user api for HTTPS Status codes

		Asserts:
			HTTPStatus CREATED from the response code
			Response body containing "access_token"
		"""
		data = {"username": USERNAME, "pass1": PASSWORD}
		with response in e
		response = self.darwin_client.create_post(data, AUTH_LOGIN_USER)
		
		assert response.status_code == HTTPStatus.CREATED
		assert "access_token" in response.text
	
	@pytest.mark.tags('test_darwin')
	def test_login(self):
		"""
		Negative testing with Darwin /auth/login api

		Asserts:
			HTTPSStatus code of 401 BAD Request.
		"""
		data = {'api-key': self.get_access_token(), 'password': PASSWORD}
		response = self.darwin_client.create_post(data, AUTH_LOGIN)
		
		assert response.status_code == HTTPStatus.BAD_REQUEST
		assert "Incorrect username or password" in response.json()['message']
	
	def get_access_token(self):
		"""To get the access token when user logs in"""
		
		data = {'username': USERNAME, 'pass1': PASSWORD}
		response = self.darwin_client.create_post(data, AUTH_LOGIN_USER)
		return response.json()['access_token']
