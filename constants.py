"""Constants for the Darwin APIs"""
import os
# Configuration File Constant
CONFIG_FILE = '../conf/url.ini'

# DataSet File Constant
DATASET_FOLDER = '../sdk/testdata/'

# Darwin APIs
AUTH_LOGIN_USER = 'auth/login/user'
AUTH_LOGIN = 'auth/login'

USERNAME = os.environ.get('username')
PASSWORD = os.environ.get('pass1')