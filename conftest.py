# contest of configuration.

import pytest


def pytest_addoption(parser):
	"""
	Pytest passing different command line options to a test function

	Args:
		parser: Command line parser

	References:
		https://docs.pytest.org/en/latest/example/simple.html

	"""
	parser.addoption(
			"--env",
			action="store",
			default="qa",
			help="Environments to run test: qa"
	)


@pytest.fixture(scope='session')
def env(request) -> str:
	"""Environment to run tests: qa or stage.
	Determined by --env command line option. Environments must be defined in url.ini..

	Args:
		request: Request variable for environment

	Returns:
		given_env: Selected Environment.

	"""
	valid_envs = ['qa', 'dev']
	given_env = request.config.option.env
	assert given_env in valid_envs, """No environment 
	or invalid environment given as command line option. 
	Valid environments: qa and dev"""

	return given_env
