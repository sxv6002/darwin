import configparser
from constants import CONFIG_FILE
from urllib.parse import urljoin

import requests


class DarwinClient:
	""" Darwin API Client configuration details."""
	
	def __init__(self, env: str):
		"""
		Initializing the Client service with the base url with the given environment.
			Args:
				env: Given Environment.
		"""
		config_loader = configparser.ConfigParser()
		config_loader.read_file(open(CONFIG_FILE))
		self.env = env
		self.darwin_url = config_loader[env]['darwin_base_url']
	
	def create_post(self, data, tail_end) -> requests.Response:
		"""
		Creating a Post in Darwin
		Args:
			data: data required for the APIs
			tail_end: Tail end of Darwin API

		Returns:
			response: Response object from the Darwin APIs

		"""
		response = requests.post(
			url=self.build_url(self.darwin_url, tail_end),
			data=data
		)
		response.raise_for_status()
		return response
	
	@staticmethod
	def build_url(base_url, tail_end):
		return urljoin(base_url, tail_end)
